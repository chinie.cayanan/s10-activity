package com.zuitt.discussion;

public class Post {
    //Properties
    private String title;

    //constructors
    public Post(){}
    public Post(String title, String content){
        this.title = title;
    }

    //getters
    public String getTitle(){
        return title;
    }
}
