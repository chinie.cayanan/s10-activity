package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//Retrieving all posts file
	//http://localhost:8080/posts

//	@RequestMapping(value = "posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts(){
		return "All posts retrieved";
	}

	//Creating a new post
//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
		public String createPosts(){
			return "New Posts Created";
		}

//	Retrieving A single post
//	@RequestMapping(value = "posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPosts(@PathVariable long postid){
		return "Viewing details of posts " + postid;
	}

//	Deleting a post
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping ("/posts/{postid}")
	public String deletePosts(@PathVariable long postid){
		return "The post " + postid +  " has been deleted";
	}

//	updating post
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	@PutMapping ("/posts/{postid}")
	@ResponseBody
//	ResponseBody annotation allows the response to be sent back to the client in JSON format
	public Post updatePost(@PathVariable long postid, @RequestBody Post post){
		return post;
	}

//	retrieving posts of a particular user
//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	@GetMapping ("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Post for "+ user + " have been retrieved";
	}

	//ACTIVITY S10

	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	@PostMapping("/users")
	public String postUser(){
		return "New user created";
	}

	@GetMapping("/users/{user}")
	public String getUser(@PathVariable String user){
		return "name: "+user;
	}

	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable String userid,@RequestHeader(value="authorization") String user){
		if(user.isEmpty()){
			user = "Unauthorized access";
		}
		else {
			user = "The user " + userid + " has been deleted";
		}
		return user;
	}

	@PutMapping("/users/{userid}")
	@ResponseBody
	public User putUser(@PathVariable String userid,@RequestBody User user){
		return user;
	}
}

